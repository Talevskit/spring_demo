package com.example.demo.student.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.infrastructure.EndPoints;
import com.example.demo.student.dto.StudentDto;
import com.example.demo.student.service.impl.StudentServiceImpl;

@RequestMapping(EndPoints.STUDENTS)
@RestController
@CrossOrigin
public class StudentController {

	@Autowired
	private StudentServiceImpl studentService;
	
	@GetMapping("/{id}")
	public StudentDto getById(@PathVariable("id") Integer id) {
		return studentService.findById(id);
	}
	
	@GetMapping
	public List<StudentDto> getAll(){
		return studentService.getAll();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public StudentDto createStudent(@RequestBody StudentDto studentDto) {
		return studentService.createStudent(studentDto);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public StudentDto updateStudent(@PathVariable("id") Integer id, @RequestBody StudentDto studentDto) {
		return studentService.updateStudent(id, studentDto);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeStudent(@PathVariable("id") Integer id) {
		studentService.removeStudent(id);
	}
	

}
