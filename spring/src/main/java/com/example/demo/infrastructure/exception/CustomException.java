package com.example.demo.infrastructure.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class CustomException {
	
	private String message;
	private HttpStatus httpStatus;
	private LocalDateTime timestamp;

}
