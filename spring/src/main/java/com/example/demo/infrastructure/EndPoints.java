package com.example.demo.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EndPoints {
	public static final String BASE = "/api/";
	public static final String STUDENTS = BASE + "students/";
	
}
