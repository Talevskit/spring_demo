package com.example.demo.student.dto;

import com.example.demo.infrastructure.dto.BaseDto;

import lombok.Data;
@Data
public class StudentDto extends BaseDto {
	
	private String firstName;
	private String lastName;

}
