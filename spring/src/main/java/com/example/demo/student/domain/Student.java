package com.example.demo.student.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.example.demo.infrastructure.domain.BaseStudent;

import lombok.Data;

@Data
@Entity
@Table(name="students")
public class Student extends BaseStudent {
	private static final long serialVersionUID = 1L;
	
	@Column
	private String firstName;
	@Column
	private String lastName;

}
