import React, { useEffect } from "react";
import { deleteStudent, getStudents } from "../actions/studentActions";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

const StudentList = () => {
  let history = useHistory();
  const dispatch = useDispatch();
  const students = useSelector((state) => state.students.students);

  useEffect(() => {
    dispatch(getStudents());
  }, []);
  console.log(students);
  return (
    <div className="container">
      <button
        className="btn btn-secondary my-3"
        onClick={() => history.push("/student/-1")}
      >
        Add Student
      </button>
      <table className="table table-hover table-striped table-bordered table-dark">
        <thead>
          <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {students.map((student) => (
            <tr key={student.id}>
              <td>{student.firstName}</td>
              <td>{student.lastName}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => dispatch(deleteStudent(student.id))}
                >
                  Delete
                </button>
                <button
                  className="btn btn-secondary ml-2"
                  onClick={() => history.push(`/student/${student.id}`)}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default StudentList;
