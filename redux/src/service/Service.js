import axios from "axios";

const API = "http://localhost:8091/api";

export default axios.create({ baseURL: API });
