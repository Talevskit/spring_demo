import axios from "../service/Service";
import {
  GET_STUDENTS,
  GET_STUDENT,
  DELETE_STUDENT,
  ADD_STUDENT,
  UPDATE_STUDENT,
} from "./types";

// export const getStudents = () => (dispatch) => {
//   fetch("http://localhost:8091/api/students/")
//     .then((res) => res.json())
//     .then((data) =>
//       dispatch({
//         type: GET_STUDENTS,
//         payload: data,
//       })
//     );
// };

export const getStudents = () => (dispatch) => {
  return axios.get("/students/").then(({ data }) => {
    dispatch({
      type: GET_STUDENTS,
      payload: data,
    });
  });
};

export const getStudent = (id) => (dispatch) => {
  return axios.get(`/students/${id}`).then(({ data }) => {
    dispatch({
      type: GET_STUDENT,
      payload: data,
    });
  });
};

export const addStudent = (student) => (dispatch) => {
  return axios.post("/students/", student).then(({ data }) => {
    dispatch({
      type: ADD_STUDENT,
      payload: data,
    });
  });
};

export const deleteStudent = (id) => (dispatch) => {
  return axios.delete(`/students/${id}`).then((res) => {
    dispatch({
      type: DELETE_STUDENT,
      payload: id,
    });
  });
};

export const updateStudent = (id, student) => (dispatch) => {
  return axios.put(`/students/${id}`, student).then(({ data }) => {
    dispatch({
      type: UPDATE_STUDENT,
      payload: data,
    });
  });
};

export default getStudents;
