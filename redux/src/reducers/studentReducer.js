import {
  GET_STUDENTS,
  DELETE_STUDENT,
  GET_STUDENT,
  ADD_STUDENT,
  UPDATE_STUDENT,
} from "../actions/types";

const initialState = {
  students: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_STUDENTS:
      return {
        ...state,
        students: action.payload,
      };
    case GET_STUDENT:
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case ADD_STUDENT:
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case DELETE_STUDENT:
      return {
        ...state,
        students: state.students.filter((stud) => stud.id !== action.payload),
      };
    case UPDATE_STUDENT:
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    default:
      return state;
  }
};
