package com.example.demo.student.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.infrastructure.exception.ApiException;
import com.example.demo.student.domain.Student;
import com.example.demo.student.dto.StudentDto;
import com.example.demo.student.mapper.StudentMapper;
import com.example.demo.student.repo.StudentRepo;
import com.example.demo.student.service.StudentService;


@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	StudentRepo studentRepo;
	@Autowired
	StudentMapper studentMapper;

	@Override
	public StudentDto findById(Integer id) {
		Student student = studentRepo.findById(id).orElseThrow(() -> {
			return new ApiException("Resource not found");
		});
		return studentMapper.entityToDto(student);
	}

	@Override
	public StudentDto createStudent(StudentDto studentDto) {
		Student transientStudent = studentMapper.dtoToEntity(studentDto);
		Student persistedStudent = studentRepo.save(transientStudent);
		return studentMapper.entityToDto(persistedStudent);
	}

	@Override
	public StudentDto updateStudent(Integer id, StudentDto studentDto) {
		Student persistedStudent = studentRepo.findById(id).get();
		studentMapper.mapRequestedFieldForUpdate(studentDto, persistedStudent);
		return studentMapper.entityToDto(studentRepo.saveAndFlush(persistedStudent));
	}

	@Override
	public List<StudentDto> getAll() {
		return studentMapper.mapList(studentRepo.findAll(), StudentDto.class);
	}

	@Override
	public void removeStudent(Integer id) {
		Student persistedStudent = studentRepo.findById(id).get();
		studentRepo.delete(persistedStudent);
		
	}
	
	

}
