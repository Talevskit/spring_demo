package com.example.demo.infrastructure.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {
	@ExceptionHandler(value= {ApiException.class})
	public ResponseEntity<Object> handleApiException(ApiException apiException){
		
		HttpStatus badRequest = HttpStatus.BAD_REQUEST;
		CustomException badRequestException = new CustomException(apiException.getMessage(), badRequest, LocalDateTime.now());
	
	return new ResponseEntity<>(badRequestException, badRequest);
	}

}
