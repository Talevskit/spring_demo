package com.example.demo.student.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.infrastructure.mapper.AbstractGeneralMapper;
import com.example.demo.student.domain.Student;
import com.example.demo.student.dto.StudentDto;
import com.example.demo.student.mapper.StudentMapper;

@Component
public class StudentMapperImpl extends AbstractGeneralMapper implements StudentMapper{

	@Autowired
	public StudentMapperImpl(ModelMapper modelMapper) {
		super(modelMapper);
	}

	@Override
	public StudentDto entityToDto(Student student) {
		return this.modelMapper.map(student, StudentDto.class);
	}

	@Override
	public Student dtoToEntity(StudentDto dto) {
		return this.modelMapper.map(dto, Student.class);
	}

	@Override
	public void mapRequestedFieldForUpdate(StudentDto dto, Student student) {
		student.setFirstName(dto.getFirstName());
		student.setLastName(dto.getLastName());
		
	}

}
