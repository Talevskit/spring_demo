package com.example.demo.student.mapper;

import com.example.demo.infrastructure.mapper.GeneralMapper;
import com.example.demo.student.domain.Student;
import com.example.demo.student.dto.StudentDto;

public interface StudentMapper extends GeneralMapper<StudentDto, Student> {
	public void mapRequestedFieldForUpdate(StudentDto dto, Student student);
}
