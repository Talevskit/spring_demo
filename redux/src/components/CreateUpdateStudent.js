import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { addStudent, updateStudent } from "../actions/studentActions";
import { useDispatch } from "react-redux";

const CreateUpdateStudent = () => {
  let history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const { id } = useParams();
  const dispatch = useDispatch();

  const getTitle = () => {
    if (id <= 0) {
      return <h3>Create Student</h3>;
    } else {
      return <h3>Update Student</h3>;
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let student = {
      firstName: firstName,
      lastName: lastName,
    };
    if (id <= 0) {
      dispatch(addStudent(student));
    } else {
      dispatch(updateStudent(id, student));
    }
    history.push("/students/");
  };

  return (
    <div className="container p-3 my-3 bg-dark text-white">
      {getTitle()}
      <form onSubmit={handleSubmit}>
        <h3 className="mt-3">First name</h3>
        <input
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        ></input>
        <h3>Last name</h3>
        <input
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        ></input>
        <br />
        <div className="mt-3">
          <button className="btn btn-primary" type="submit">
            Save
          </button>
          <button
            className="btn btn-secondary ml-2"
            type="button"
            onClick={() => history.push("/students/")}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateUpdateStudent;
