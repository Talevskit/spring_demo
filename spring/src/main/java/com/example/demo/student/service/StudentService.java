package com.example.demo.student.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.student.dto.StudentDto;

@Service
public interface StudentService {

	public StudentDto findById(Integer id);

	public StudentDto createStudent(StudentDto studentDto);

	public StudentDto updateStudent(Integer id, StudentDto studentDto);

	public List<StudentDto> getAll();

	public void removeStudent(Integer id);

}
