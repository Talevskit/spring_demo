import "./App.css";
import StudentList from "./components/StudentList";
import CreateUpdateStudent from "./components/CreateUpdateStudent";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <div className="container">
          <Switch>
            <Route path="/students" component={StudentList}></Route>
            <Route path="/student/:id" component={CreateUpdateStudent}></Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
